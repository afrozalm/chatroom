import socket
import select
import sys
import getpass


class Client(object):
    def __init__(self, ip_addr, port, buffSize=2048):
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            server.connect((ip_addr, port))
        except socket.error:
            print 'Server not found. Shutting down'
            exit(0)
        self.server = server
        self.buffSize = buffSize
        self.lastCMD = ''
        self.validCommands = ['p2p', 'online', 'broadcast',
                              'logout', 'read', '1', '2']

    def authenticate(self):
        success = False
        response = ''
        while not success:
            # Username Prompt

            if 'Username' not in response:
                response = self.server.recv(self.buffSize)
                if 'FAIL' in response:
                    print response
                    return False

                sys.stdout.write(response)
                sys.stdout.flush()
            uname = sys.stdin.readline()
            self.server.send(uname.strip())

            response = self.server.recv(self.buffSize)
            if 'ERR' in response:
                sys.stdout.write(response)
                sys.stdout.flush()
                continue
            if 'FAIL' in response:
                print response
                return False

            # Password Prompt
            sys.stdout.write(response)
            sys.stdout.flush()
            password = getpass.getpass(prompt='')
            self.server.send(password.strip())

            response = self.server.recv(self.buffSize)
            if 'SUCCESS' in response:
                success = True
            if 'ERR' in response:
                sys.stdout.write(response)
                sys.stdout.flush()

            if '[FAIL]' in response:
                print response
                return False

        return True

    def getCMDprefix(self, message):
        if message == 'logout':
            return 'logout', True

        if 'p2p' in message.split(' ')[0]:
            try:
                uname = message.split(' ')[1]
                return ' '.join(['p2p', uname]), True
            except:
                return self.lastCMD, False

        if 'online' == message:
            return 'online', True

        if 'broadcast' == message.split(' ')[0]:
            return 'broadcast', True

        if 'read' == message.split(' ')[0]:
            try:
                opt = message.split(' ', 2)[1]
                return ' '.join(['read', opt]), True
            except:
                return self.lastCMD, False

        if '1' == message:
            return '1', True
        if '2' == message:
            return '2', True

        return self.lastCMD, False

    def analyse(self, message):
        if self.lastCMD == '':
            self.lastCMD, _ = self.getCMDprefix(message)

        for cmd in self.validCommands:
            if cmd in message:
                self.lastCMD, valid = self.getCMDprefix(message)
                if valid:
                    return message
                else:
                    return self.lastCMD + ' ' + message
                break

        return self.lastCMD + ' ' + message

    def start(self):
        try:
            if not self.authenticate():
                self.shutdown()
                return
        except socket.error:
            self.shutdown()
            return
        except KeyboardInterrupt:
            self.shutdown()
            return

        sys.stdout.write("Press [Enter] to continue\n")
        sys.stdout.flush()
        while True:
            try:
                sockets_list = [sys.stdin, self.server]
                read_sockets, write_socket, error_socket =\
                    select.select(sockets_list, [], [], 0.01)

                for socks in read_sockets:
                    if socks == self.server:
                        message = socks.recv(self.buffSize)
                        if '>' in message.split(' ', 1)[0]:
                            sys.stdout.write('\r' + message)
                        else:
                            sys.stdout.write('\r> ' + message)
                        sys.stdout.flush()
                    else:
                        sys.stdout.write("YOU> ")
                        sys.stdout.flush()
                        message = sys.stdin.readline().strip()
                        message = self.analyse(message).strip()
                        self.server.send(message)
                        if message == 'logout':
                            self.shutdown()
            except socket.error:
                self.shutdown()
                break
            except KeyboardInterrupt:
                self.shutdown()
                break

    def shutdown(self):
        print 'Shutting down'
        self.server.close()


def main():
    if len(sys.argv) != 3:
        print "Correct usage: script, IP address, port number"
        exit()
    IP_address = str(sys.argv[1])
    Port = int(sys.argv[2])
    c = Client(IP_address, Port)
    c.start()


if __name__ == '__main__':
    main()
