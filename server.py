import socket
import sys
import os
import os.path
from thread import start_new_thread
import datetime
import time


class Server(object):
    def __init__(self, ip_addr, port_no,
                 buffSize=2048, fname='users.txt'):

        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind((ip_addr, port_no))
        server.listen(100)
        self.fname = fname
        self.buffSize = buffSize
        self.server = server
        self.list_of_clients = []
        self.active_clients = set()
        self.waiting2connet = set()
        self.uname2conn = {}
        self.conn2uname = {}
        self.getAllUsers()
        self.blockedIPs = {}
        self.auth_trials = 3
        self.auth_timer = datetime.timedelta(0, 60, 0)
        return

    def getAllUsers(self):
        self.allUsers = []
        with open(self.fname) as f:
            lines = f.readlines()

        for line in lines:
            self.allUsers.append(line.strip().split(':')[0])

    def Match(self, uname, password):
        with open(self.fname) as f:
            lines = f.readlines()
        for line in lines:
            curr_name, curr_password = line.strip().split(':')
            if curr_name == uname:
                if password == curr_password:
                    return True, True
                else:
                    return True, False

        return False, False

    def authenticate(self, conn, addr):
        try:
            conn.send("\nUsername: ")
            uname = conn.recv(self.buffSize)

            while uname in self.waiting2connet:
                time.sleep(1)

            self.lastadded = uname
            self.waiting2connet.add(uname)
            if uname in self.active_clients:
                conn.send('SERVER> [ERR1]: ' +
                          uname + " already logged in." +
                          " Please logout from other sources and proceed again"
                          )
                return False

            conn.send("Password: ")
            password = conn.recv(self.buffSize)
            print 'password recvd'
            a, b = self.Match(uname, password)
            if not a:
                print 'invalid uname', uname
                conn.send("SERVER> [ERR2]: Invalid username " + uname)
                return False
            if not b:
                print 'invalid password'
                conn.send("SERVER> [ERR3]: Invalid password.")
                return False

            conn.send("SERVER> [SUCCESS]: Logged in as " + uname)
            self.active_clients.add(uname)
            self.waiting2connet.remove(uname)
            self.uname2conn[uname] = conn
            self.conn2uname[conn] = uname
            print uname, 'logged in'
            return 'success'
        except socket.error:
            try:
                self.waiting2connet.remove(self.lastadded)
            except:
                pass
            self.remove(conn)
            return 'closed'

    def logout(self, connection):
        self.remove(connection)
        try:
            uname = self.conn2uname[connection]
            self.active_clients.remove(uname)
            del self.uname2conn[uname]
            del self.conn2uname[connection]
            print 'logging out', uname
        except KeyError:
            pass

    def clientthread(self, conn, addr):
        success = False
        for _ in xrange(self.auth_trials):
            status = self.authenticate(conn, addr)
            if status == 'success':
                success = True
                break
            if status == 'closed':
                return

        try:
            if not success:
                ip = addr[0]
                self.blockedIPs[ip] = datetime.datetime.now()
                conn.send('.\nSERVER> [FAIL]: You expired your authentication trials.'
                          + ' Your IP has been blocked for 60 seconds.\n')
                self.remove(conn)
                return

            self.handleNewConnection(conn)
        except socket.error:
            self.logout(conn)

        while True:
            try:
                message = conn.recv(2048)
                if message:
                    self.handle(message, conn)
                else:
                    self.logout(conn)

            except socket.error:
                conn.close()
                break

    def pendingUsers(self, messages):
        names = set()
        for message in messages:
            curr_name = message.split('>', 1)[0]
            if names != 'YOU':
                names.add(curr_name)

        return ', '.join(list(names))

    def handleNewConnection(self, connection):
        fname = './pendingMessages/' + self.conn2uname[connection] + '.dat'
        if os.path.isfile(fname):
            connection.send(
                "SERVER> You have new messages." +
                " Would you like to read them now[1] or later[2]\nYOU> ")
            response = connection.recv(self.buffSize)
            if response == '1':
                self.read(connection)

    def handle(self, message, conn):
        '''
        Can handle 5 types of messages:
        - p2p       :  p2p <uname> <message>
        - broadcast :  broadcast <message>
        - logout    :  logout
        - online    :  online
        - read      : read <all | uname>
        '''
        cmd = message.split(' ', 1)[0]
        print cmd
        if cmd == 'logout':
            self.logout(conn)
        elif cmd == 'p2p':
            try:
                target_user, target_message = message.split(' ', 2)[1:]
                self.p2p(conn, target_user, target_message + '\nYOU> ')
            except:
                if len(message.split()) == 2:
                    conn.send("SERVER> p2p initiated with "
                              + message.split()[1] + '\nYOU> ')
                else:
                    conn.send("SERVER> Usage: p2p <uname> <message>\nYOU> ")
                pass
        elif cmd == 'broadcast':
            target_message = message.split(' ', 1)[1]
            self.broadcast(target_message + '\nYOU> ', conn)
        elif cmd == 'online':
            self.online(conn)
        elif cmd == 'read':
            self.read(conn)
        else:
            conn.send("SERVER> " + cmd + ' not recognized\nYOU> ')

        return

    def p2p(self, sender_conn, target_user, target_message):
        sender_uname = self.conn2uname[sender_conn]
        if target_user not in self.allUsers:
            sender_conn.send("SERVER> " + target_user
                             + ' does not exist in this platform.\nYOU> ')
            return

        target_conn = self.uname2conn.get(target_user, None)
        if target_conn is None:
            f = open('./pendingMessages/' + target_user + '.dat', 'a')
            f.write(sender_uname + '> ' + target_message[:-5])
            f.close()
        else:
            try:
                target_conn.send(sender_uname + '> ' + target_message)
            except:
                self.logout(target_conn)

    def broadcast(self, message, connection):
        sender = self.conn2uname[connection]
        for user in self.allUsers:
            if user != sender:
                self.p2p(sender_conn=connection,
                         target_user=user,
                         target_message=message)

    def online(self, connection):

        res = '\n> '.join([uname for uname in self.active_clients])
        try:
            connection.send('> ' + res + '\nYOU> ')
        except socket.error:
            self.logout(connection)

    def read(self, connection):
        fname = './pendingMessages/' + self.conn2uname[connection] + '.dat'
        if not os.path.isfile(fname):
            connection.send('SERVER> You have no unread messages.\nYOU> ')
            return

        with open(fname) as f:
            messages = f.readlines()

        pendingUsers = self.pendingUsers(messages)
        if len(pendingUsers) == 0:
            os.remove(fname)
            return

        prefix = ' has' if len(pendingUsers.split()) == 1 else ' have'
        connection.send("SERVER> " +
                        pendingUsers + prefix + ' sent you message(s).\nYOU> ')

        response = connection.recv(self.buffSize)
        if response == 'read all':
            allmessages = '\n'.join(messages)
            connection.send(allmessages)
            os.remove(fname)
        else:
            requestedName = response.split()[1]
            l = len(requestedName)
            requestedMessage = requestedName + '>'
            restoreMessages = ''
            for message in messages:
                if message[:l + 1] == requestedName + '>':
                    requestedMessage += message[l + 1:]
                else:
                    restoreMessages += message

            connection.send(requestedMessage + '\nYOU> ')
            del messages

            print '>', restoreMessages, '<'
            if requestedMessage.strip() == '':
                os.remove(fname)
            else:
                f = open(fname, 'w')
                f.write(restoreMessages)
                f.close()

    def remove(self, connection):
        if connection in self.list_of_clients:
            self.list_of_clients.remove(connection)

        connection.close()

    def start(self):
        while True:
            try:
                conn, addr = self.server.accept()
                ip = addr[0]
                if ip in self.blockedIPs:
                    now = datetime.datetime.now()
                    if now - self.blockedIPs[ip] > self.auth_timer:
                        del self.blockedIPs[ip]
                    else:
                        print 'blocked', addr
                        conn.close()
                        continue
                self.list_of_clients.append(conn)
                print addr[0] + " connected"

                start_new_thread(self.clientthread, (conn, addr))
            except socket.error:
                break
            except KeyboardInterrupt:
                break

        self.shutdown()

    def shutdown(self):
        print '\nShutting down'
        self.server.close()


def main():
    if len(sys.argv) != 3:
        print "Correct usage: python server.py <IP address> <port number>"
        exit()

    IP_address = str(sys.argv[1])

    # takes second argument from command prompt as port number
    Port = int(sys.argv[2])
    s = Server(IP_address, Port)
    s.start()


if __name__ == '__main__':
    main()
